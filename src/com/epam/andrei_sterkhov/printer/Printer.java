package com.epam.andrei_sterkhov.printer;

import com.epam.andrei_sterkhov.calculator.Calculator;
import com.epam.andrei_sterkhov.educational_center.Student;

import java.util.List;

public class Printer {
    private Calculator calculator = new Calculator();

    public void isStudentContinueEducation(Student student) {
        System.out.printf("%-15s %-8s До окончания обучения по программе %-25s осталось %-2d д. Средний балл %3.2f. %s.%n",
                student.getSurname(), student.getName(), student.getTraining().getName(),
                calculator.getDaysToFinish(student), calculator.getMiddleMark(student),
                calculator.isContinueEducation(student, 4.5) ? "Может продолжать обучение" : "Отчислить");
    }

    public void studentsByMiddleMark(List<Student> students) {
        students.forEach(student -> System.out.printf("Student: %-15s %-8s Middle Mark: %.2f%n",
                student.getSurname(), student.getName(), calculator.getMiddleMark(student)));
    }

    public void studentsByDaysToFinish(List<Student> students) {
        students.forEach(student -> System.out.printf("Student: %-15s %-8s До окончания обучения: %d дней.%n",
                student.getSurname(), student.getName(), calculator.getDaysToFinish(student)));
    }

    public void printStudents(List<Student> students) {
        students.forEach(this::isStudentContinueEducation);
    }
}
