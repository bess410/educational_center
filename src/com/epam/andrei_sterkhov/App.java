package com.epam.andrei_sterkhov;

import com.epam.andrei_sterkhov.calculator.Calculator;
import com.epam.andrei_sterkhov.educational_center.EducationalCenter;
import com.epam.andrei_sterkhov.generator.Generator;
import com.epam.andrei_sterkhov.printer.Printer;

public class App {
    public static void main(String[] args) {
        Generator gen = new Generator();
        Printer printer = new Printer();
        Calculator calculator = new Calculator();
        // Получаем рандомный учебный центр
        EducationalCenter center = gen.getRandomEducationalCenter();
        System.out.println("------------------");
        System.out.println("Получаем всех студентов: ");
        System.out.println("------------------");
        printer.printStudents(center.getStudents());
        System.out.println("------------------");
        System.out.println("Сортировка по среднему баллу:");
        System.out.println("------------------");
        printer.studentsByMiddleMark(calculator.getStudentsByMiddleMark(center.getStudents()));
        System.out.println("------------------");
        System.out.println("Сортировка по времени до окончания обучения:");
        System.out.println("------------------");
        printer.studentsByDaysToFinish(calculator.getStudentsByDaysToFinish(center.getStudents()));
        System.out.println("------------------");
        System.out.println("Студенты, которые могут продолжать обучение");
        System.out.println("------------------");
        printer.printStudents(calculator.getStudentsContinuedEducation(center.getStudents()));
    }
}
