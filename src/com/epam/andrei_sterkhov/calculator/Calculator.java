package com.epam.andrei_sterkhov.calculator;

import com.epam.andrei_sterkhov.educational_center.Mark;
import com.epam.andrei_sterkhov.educational_center.Student;

import java.util.Comparator;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

public class Calculator {
    public int getDaysToFinish(Student student) {
        // Из общего числа дней обучения вычитаем количество оценок студента
        // так как студент получает одну оценку в день
        return student.getTraining().getCountDays() - student.getMarks().size();
    }

    public double getMiddleMark(Student student) {
        OptionalDouble average = student.getMarks()
                .stream()
                .mapToDouble(Mark::getMark)
                .average();
        if (average.isPresent()) {
            return average.getAsDouble();
        } else {
            return 0;
        }
    }

    public boolean isContinueEducation(Student student, double threshold) {
        // Получаем сумму текущих оценок студента
        int sumCurrentMarks = student.getMarks().stream().mapToInt(Mark::getMark).sum();
        // Получаем сумму оценок за оставшиеся дни, если бы студент получал за ним максимальный балл
        int sumLastMarks = getDaysToFinish(student) * Mark.FIVE.getMark();
        // Делим результат на общее количество дней
        double average = (sumCurrentMarks + sumLastMarks) / (double) student.getTraining().getCountDays();
        return average >= threshold;
    }

    public List<Student> getStudentsByMiddleMark(List<Student> students) {
        return students.stream().sorted(Comparator.comparingDouble(this::getMiddleMark)).collect(Collectors.toList());
    }

    public List<Student> getStudentsByDaysToFinish(List<Student> students) {
        return students.stream().sorted(Comparator.comparing(this::getDaysToFinish)).collect(Collectors.toList());
    }

    public List<Student> getStudentsContinuedEducation(List<Student> students) {
        return students.stream().filter(student -> this.isContinueEducation(student, 4.5)).collect(Collectors.toList());
    }
}
