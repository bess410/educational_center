package com.epam.andrei_sterkhov.generator;

import com.epam.andrei_sterkhov.educational_center.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Generator {
    private Random random = new Random();

    private Course getRandomCourse() {
        Course course = new Course();
        String[] courseNames = {"Технология Java Servlets", "Struts Framework", "Spring Framework",
                "Hibernate", "Обзор технологий Java", "Библиотека JFS/Swing",
                "Технология JDBC", "Технология JAX", "Библиотеки commons"};
        String name = courseNames[random.nextInt(courseNames.length)];
        course.setName(name);
        // Устанавливаем длительность обучения кратной 8-ми часам
        // или считаем, что каждый курс начинается с нового дня
        course.setDuration((random.nextInt(10) + 1) * 8);
        return course;
    }

    public Training getRandomTraining() {
        Training training = new Training();
        String[] trainingNames = {"J2EE Developer", "Java Developer", "Java Junior Developer"};
        String name = trainingNames[random.nextInt(trainingNames.length)];
        training.setName(name);
        // Вносим от 3 до 10 курсов в тренинг
        List<Course> courses = new ArrayList<>();
        for (int i = 0; i < (random.nextInt(8) + 2); i++) {
            courses.add(getRandomCourse());
        }
        training.setCourses(courses);
        return training;
    }

    public Student getRandomStudent() {
        Student student = new Student();
        String[] names = {"Саша", "Петя", "Вася", "Коля", "Глеб", "Витя", "Вадим", "Андрей", "Стас", "Влад", "Филипп"};
        String name = names[random.nextInt(names.length)];
        student.setName(name);

        String[] surnames = {"Иванов", "Петров", "Обломов", "Фоменко", "Самойлов", "Цой", "Макаревич",
                "Михайлов", "Сташевский", "Киркоров"};
        String surname = surnames[random.nextInt(surnames.length)];
        student.setSurname(surname);

        Training training = getRandomTraining();
        student.setTraining(training);

        int lastDays = random.nextInt(training.getCountDays());
        for (int i = 0; i < lastDays; i++) {
            student.addMark(Mark.values()[random.nextInt(Mark.values().length)]);
        }
        LocalDate startEducation = LocalDate.now().minusDays(lastDays);
        student.setStartEducation(startEducation);
        return student;
    }

    public EducationalCenter getRandomEducationalCenter() {
        EducationalCenter center = new EducationalCenter();
        // Вносим от 5 до 20 студентов в учебный центр
        List<Student> students = new ArrayList<>();
        for (int i = 0; i < (random.nextInt(16) + 5); i++) {
            students.add(getRandomStudent());
        }
        center.setStudents(students);
        return center;
    }
}