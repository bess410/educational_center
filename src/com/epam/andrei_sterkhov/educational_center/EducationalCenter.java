package com.epam.andrei_sterkhov.educational_center;

import java.util.List;

public class EducationalCenter {
    private List<Student> students;

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "EducationalCenter{" +
                "students=" + students +
                '}';
    }
}
