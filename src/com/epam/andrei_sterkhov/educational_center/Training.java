package com.epam.andrei_sterkhov.educational_center;

import java.util.List;

public class Training {
    private String name;
    private List<Course> courses;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    // Количество оценок в данном тренинге
    public int getCountDays() {
        return courses.stream().mapToInt(course -> course.getDuration() / 8).sum();
    }

    @Override
    public String toString() {
        return "Training{" +
                "name='" + name + '\'' +
                ", courses=" + courses +
                '}';
    }
}
