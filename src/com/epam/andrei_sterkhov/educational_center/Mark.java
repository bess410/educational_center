package com.epam.andrei_sterkhov.educational_center;

public enum Mark {
    ONE(1), TWO(2), THREE(3), FOUR(4), FIVE(5);

    private int mark;

    Mark(int mark) {
        this.mark = mark;
    }

    public int getMark() {
        return mark;
    }

    @Override
    public String toString() {
        return Integer.toString(getMark());
    }
}
